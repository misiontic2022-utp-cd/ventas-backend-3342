package co.edu.utp.misiontic.cesardiaz.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.utp.misiontic.cesardiaz.controller.request.CalcularCuotaRequest;
import co.edu.utp.misiontic.cesardiaz.controller.response.CalcularCuotaResponse;
import co.edu.utp.misiontic.cesardiaz.service.VentasService;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("api/sales")
@Slf4j
public class VentaController {

    private final VentasService service;

    public VentaController(VentasService service) {
        this.service = service;
    }

    @GetMapping
    public String hello(){
        log.info("Hello");
        return "Hola Mundo";
    }

    @PostMapping("/part")
    public ResponseEntity<CalcularCuotaResponse> calcularCuota(@RequestBody CalcularCuotaRequest body) {
        log.info("Calcular Cuota");
        var response = new CalcularCuotaResponse();
        try {
            var valor = service.calcularValorCuota(body);

            response.setValor(valor);
            return ResponseEntity.ok(response);
        } catch (Exception ex) {
            response.setError(ex.getMessage());
            return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
        }
    }

}