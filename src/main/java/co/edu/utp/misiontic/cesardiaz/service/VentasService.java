package co.edu.utp.misiontic.cesardiaz.service;

import co.edu.utp.misiontic.cesardiaz.controller.request.CalcularCuotaRequest;

public interface VentasService {
    Integer calcularValorCuota(CalcularCuotaRequest request);
}
