package co.edu.utp.misiontic.cesardiaz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VentasBackend3342Application {

	public static void main(String[] args) {
		SpringApplication.run(VentasBackend3342Application.class, args);
	}

}
