package co.edu.utp.misiontic.cesardiaz.controller.request;

import java.io.Serializable;

public class CalcularCuotaRequest implements Serializable {
    
    private Integer monto;
    private Integer numeroCuotas;
    private String periodicidad;

    public Integer getMonto() {
        return monto;
    }
    public void setMonto(Integer monto) {
        this.monto = monto;
    }
    public Integer getNumeroCuotas() {
        return numeroCuotas;
    }
    public void setNumeroCuotas(Integer numeroCuotas) {
        this.numeroCuotas = numeroCuotas;
    }
    public String getPeriodicidad() {
        return periodicidad;
    }
    public void setPeriodicidad(String periodicidad) {
        this.periodicidad = periodicidad;
    }
}
