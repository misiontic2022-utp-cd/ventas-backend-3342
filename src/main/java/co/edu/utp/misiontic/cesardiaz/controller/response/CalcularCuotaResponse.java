package co.edu.utp.misiontic.cesardiaz.controller.response;

import java.io.Serializable;

public class CalcularCuotaResponse implements Serializable {
    private Integer valor;
    private String error;

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
