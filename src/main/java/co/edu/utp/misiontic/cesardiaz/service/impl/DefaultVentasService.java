package co.edu.utp.misiontic.cesardiaz.service.impl;

import org.springframework.stereotype.Service;

import co.edu.utp.misiontic.cesardiaz.controller.request.CalcularCuotaRequest;
import co.edu.utp.misiontic.cesardiaz.service.VentasService;

@Service
public class DefaultVentasService implements VentasService {

    @Override
    public Integer calcularValorCuota(CalcularCuotaRequest request) {
        if (request.getMonto() == 0) {
            throw new RuntimeException("Error en valor de monto");
        }
        if (request.getNumeroCuotas() == 0) {
            throw new RuntimeException("Error en valor de numero de cuotas");
        }

        var interes = porcentajeInteres(request.getPeriodicidad());
        var total = request.getMonto() * (100 + interes) / 100d;
        Double cuota = Math.ceil(total / request.getNumeroCuotas());

        return cuota.intValue();
    }

    private Double porcentajeInteres(String periodicidad) {
        var valor = 0d;

        switch (periodicidad) {
            case "Diario":
                valor = 1d;
                break;
            case "Semanal":
                valor = 2d;
                break;
            case "Quincenal":
                valor = 3d;
                break;
            case "Mensual":
                valor = 4d;
                break;
            default:
                throw new RuntimeException("Opcion de periodicidad no válida");
        }

        return valor;
    }

}
